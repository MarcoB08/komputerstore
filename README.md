# komputerStore

This frontend project is a website to buy a laptop.
You have to work to get a laptop of the shop.

## Buttons

buttonGetLoan : click when you want to loan some money<br>
buttonBank : click to deposit money to you balance<br>
buttonGetSalary : click to get salary<br>
buttonRepayLoan : click to deposit to your loanAmount<br>
buttonBuyLaptop : click to buy your laptop from your ballanceAmount<br>

## Add your files

```
cd existing_repo
git remote add origin https://gitlab.com/MarcoB08/komputerstore.git
git branch -M main
git push -uf origin main

```

## Members

Marco Beckers
