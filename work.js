let countBankAmount = (bankAmount, salary) => {
  bankAmount += salary;
  return bankAmount;
};

let countBankAmountWithDebitloan = (bankAmount, debitLoan) => {
  bankAmount = bankAmount - debitLoan;
  return bankAmount;
};

let countDebitLoan = (debitLoan, bankAmount) => {
  debitLoan = bankAmount * 0.1;
  return debitLoan;
};

let countBalanceAmount = (ballanceAmount, bankAmount) => {
  ballanceAmount += bankAmount;
  return ballanceAmount;
};

let countLoanAmount = (loanAmount, debitLoan) => {
  loanAmount = loanAmount - debitLoan;
  return loanAmount;
};

let countLoanAmountDepositLoan = (loanAmount, bankAmount) => {
  loanAmount -= bankAmount;
  return loanAmount;
};

let countBalanceAmountWithRemainingLoan = (ballanceAmount, loanAmount) => {
  ballanceAmount = ballanceAmount + loanAmount;
  return ballanceAmount;
};

let countNegativeLoanAmount = (loanAmount) => {
  loanAmount = loanAmount * -1;
  return loanAmount;
};

let setBankAmountToNull = (bankAmount) => {
  bankAmount = 0;
  return bankAmount;
};

let setLoanAmountToNull = (loanAmount) => {
  loanAmount = 0;
  return loanAmount;
};

let work = {
  countBankAmount,
  countBankAmountWithDebitloan,
  countDebitLoan,
  countBalanceAmount,
  countLoanAmount,
  countLoanAmountDepositLoan,
  countBalanceAmountWithRemainingLoan,
  countNegativeLoanAmount,
  setBankAmountToNull,
  setLoanAmountToNull,
};

export default work;
