// Imports
import format from "./format.js";
import bank from "./bank.js";
import work from "./work.js";

// Define all DOM Elements

// Bank
const balancedParagraph = document.getElementById("balanceText");
const loanParagraph = document.getElementById("loanText");
const loanButton = document.getElementById("buttonGetLoan");

// Work
const salaryParagraph = document.getElementById("salaryText");
const bankButton = document.getElementById("buttonBank");
const workButton = document.getElementById("buttonGetSalary");
const repayLoanButton = document.getElementById("buttonRepayLoan");

// Laptop
const laptopList = document.getElementById("laptopsList");
const specificationsParagraph = document.getElementById("specifications");

const laptopImage = document.getElementById("laptopImage");
const laptopTitle = document.getElementById("laptopTitle");
const laptopDescription = document.getElementById("laptopDescription");
const laptopPrice = document.getElementById("laptopPrice");
const BuyLaptopButton = document.getElementById("buttonBuyLaptop");

// Variables to do math or assign for functionality
let ballanceAmount = 200;
let loanAmount = 0;
let debitLoan = 0;

let bankAmount = 0;
let previousBallanceAmount = 0;
let salary = 100;

let laptops = [];
let price;

// Assign DOM Elements
balancedParagraph.innerText =
  "Balance" + " " + format.formatCurrency.format(ballanceAmount);
loanParagraph.innerText =
  "Loan" + " " + format.formatCurrency.format(loanAmount);
salaryParagraph.innerText =
  "Pay" + " " + format.formatCurrency.format(bankAmount);
specificationsParagraph.innerText = "Choose laptop";

// Fetch data of the API
fetch("https://hickory-quilled-actress.glitch.me/computers")
  .then((response) => response.json())
  .then((data) => (laptops = data))
  .then((laptops) => addLaptopsInformation(laptops));

//Functions

// Function to get loan when clicked on the loanbutton
// It checks of you have loan and checks if you have enough bankAmount to get the loan what you want.
function getLoan() {
  loanAmount = bank.getLoanfromInput(loanAmount);
  if (loanAmount <= ballanceAmount * 2 && loanAmount != 0) {
    loanParagraph.style.visibility = "visible";
    repayLoanButton.style.visibility = "visible";
    loanParagraph.innerText =
      "Loan" + " " + format.formatCurrency.format(loanAmount);
  }
}

// Function to get salary when clicked on the workbutton
// Every hit on the button counts 100 euro
function getSalary() {
  bankAmount = work.countBankAmount(bankAmount, salary);
  salaryParagraph.innerText =
    "Pay" + " " + format.formatCurrency.format(bankAmount);
}

// Function to deposit money from the bankAmount to BalanceAmount
// It checks if you have a loan or not. If you got a loan you will deposit 10% to the loanAmount and the
// remaining to the balanceAmount
function depositMoney() {
  if (loanAmount == 0) {
    ballanceAmount = work.countBalanceAmount(ballanceAmount, bankAmount);
    balancedParagraph.innerText =
      "Balance" + " " + format.formatCurrency.format(ballanceAmount);
    bankAmount = work.setBankAmountToNull(bankAmount);
    salaryParagraph.innerText =
      "Pay" + " " + format.formatCurrency.format(bankAmount);
  } else {
    debitLoan = work.countDebitLoan(debitLoan, bankAmount);
    bankAmount = work.countBankAmountWithDebitloan(bankAmount, debitLoan);
    salaryParagraph.innerText =
      "Pay" + " " + format.formatCurrency.format(bankAmount);
    loanAmount = work.countLoanAmount(loanAmount, debitLoan);
    loanParagraph.innerText =
      "Loan" + " " + format.formatCurrency.format(loanAmount);
    ballanceAmount = work.countBalanceAmount(ballanceAmount, bankAmount);
    balancedParagraph.innerText =
      "Balance" + " " + format.formatCurrency.format(ballanceAmount);
    bankAmount = work.setBankAmountToNull(bankAmount);
    salaryParagraph.innerText =
      "Pay" + " " + format.formatCurrency.format(bankAmount);
  }
}

// Function to deposit money from the bankAmount to loanAmount
// It checks if you have deposit more money then your loanAmount. If its the case then the remaining goes to the balanceAmount
function depositLoan() {
  loanAmount = work.countLoanAmountDepositLoan(loanAmount, bankAmount);
  loanParagraph.innerText =
    "Loan" + " " + format.formatCurrency.format(loanAmount);
  bankAmount = work.setBankAmountToNull(bankAmount);
  salaryParagraph.innerText =
    "Pay" + " " + format.formatCurrency.format(bankAmount);

  if (loanAmount == 0 || loanAmount < 0) {
    loanParagraph.style.visibility = "hidden";
    repayLoanButton.style.visibility = "hidden";
    if (loanAmount < 0) {
      loanAmount = work.countNegativeLoanAmount(loanAmount);
      ballanceAmount = work.countBalanceAmountWithRemainingLoan(
        ballanceAmount,
        loanAmount
      );
      balancedParagraph.innerText =
        "Balance" + " " + format.formatCurrency.format(ballanceAmount);
      loanAmount = work.setLoanAmountToNull(loanAmount);
      loanParagraph.innerText =
        "Loan" + " " + format.formatCurrency.format(loanAmount);
    }
  }
}

// All functions to get the information of the fetch of the API computers
const addLaptopsInformation = (laptops) => {
  laptops.forEach((laptop) => addLaptopToSelectMenu(laptop));
};

const addLaptopToSelectMenu = (laptop) => {
  const laptopElement = document.createElement("option");
  laptopElement.value = laptop.id;
  laptopElement.appendChild(document.createTextNode(laptop.title));
  laptopList.appendChild(laptopElement);
};

const handleInformationLaptop = (event) => {
  const selectedLaptop = laptops[event.target.selectedIndex - 1];
  specificationsParagraph.innerHTML = selectedLaptop.specs.join("<br>");
  setInformationLaptop(selectedLaptop);
};

const setInformationLaptop = (laptop) => {
  laptopImage.src = `https://hickory-quilled-actress.glitch.me/${laptop.image}`;
  laptopImage.alt = laptop.title;
  laptopTitle.style.visibility = "visible";
  laptopTitle.innerText = laptop.title;
  laptopDescription.style.visibility = "visible";
  laptopDescription.innerText = laptop.description;
  price = laptop.price;
  laptopPrice.style.visibility = "visible";
  laptopPrice.innerText = format.formatCurrency.format(price);
  BuyLaptopButton.style.visibility = "visible";
};

// Function to buy a laptop if you click on the buyButton
// Checks if you have enough money at the ballanceAmount
const buyLaptop = () => {
  previousBallanceAmount = ballanceAmount;
  previousBallanceAmount -= price;
  if (previousBallanceAmount < 0) {
    alert("Can't buy this laptop");
    previousBallanceAmount = ballanceAmount;
  } else {
    ballanceAmount = previousBallanceAmount;
    balancedParagraph.innerText =
      "Balance" + " " + format.formatCurrency.format(ballanceAmount);
    alert("You own this laptop  ");
  }
};

//EventHandlers
loanButton.addEventListener("click", getLoan);
bankButton.addEventListener("click", depositMoney);
workButton.addEventListener("click", getSalary);
repayLoanButton.addEventListener("click", depositLoan);
BuyLaptopButton.addEventListener("click", buyLaptop);
laptopList.addEventListener("change", handleInformationLaptop);
