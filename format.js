let formatCurrency = new Intl.NumberFormat("nl-NL", {
  style: "currency",
  currency: "EUR",
});

let format = {
  formatCurrency,
};

export default format;
